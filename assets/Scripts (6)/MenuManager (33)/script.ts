class MenuManagerBehavior extends Sup.Behavior {
  text : Sup.TextRenderer;
  quotes : string[];
  
  start(){
    this.text = Sup.getActor("Text").textRenderer;
    this.quotes = ["Le canard à balais","Elle a pris 20 cm","J'ai tapé dans ta mère","Et si ses parents étaient noirs","Par contre je passe sous mon père","Quand j'ai trop chaud, je l'enlève","Tape dans le fils !",
                   "Le raycasting t'a battu,\nagenouille toi devant lui","Il faut déplier les bras","C'est juste parce qu'elle est noire","Si il se fait choper dans le père...","Maman est prête à 50%","Faut définir la plus grosse quoi..",
                   "Bon... En moyenne ça fait 60%\n*calcul sur google*\nPresque... 30%"];
    Sup.Audio.playSound("Sounds/MainTheme");
  }
  update() {
    if (Sup.Input.wasMouseButtonJustPressed(0)) {
      let ray = new Sup.Math.Ray();
      ray.setFromCamera(Sup.getActor("MenuManager").camera, Sup.Input.getMousePosition()); 
      let hits = ray.intersectActors(Sup.getAllActors());
      for (let hit in hits) {
        if (hits[hit].actor.getName() == "Play") {
          Sup.loadScene("Scenes/MainScene");
        }
        else if (hits[hit].actor.getName() == "Message") {
          this.text.setText("You have no friend,\nonly your gameboy");
        }
        else if (hits[hit].actor.getName() == "Picture") {
          this.text.setText("YOU HAVE BEEN\nACTIVATED");
        }
        else if (hits[hit].actor.getName() == "Files") {
          this.text.setText(this.quotes[Math.floor(Math.random()*14)]);
        }
        else if (hits[hit].actor.getName() == "Video") {
          this.text.setText("Password required");
        }
        else if (hits[hit].actor.getName() == "Torch") {
          this.text.setText("You're gonna be detected\nif you use that app"); 
        }
        else if (hits[hit].actor.getName() == "Music") {
          this.text.setText("Lucas Lucasfleur Fleurance\nSound Design"); 
        }
        else if (hits[hit].actor.getName() == "Maps") {
          this.text.setText("Made in Nantes, 2017"); 
        }
        else if (hits[hit].actor.getName() == "Game") {
          this.text.setText("Benoît Stempi Stempin\nCorentin Hiinds Cailleaud\nCyrvan Yruam Bouchard\nJulie Kimjuri Bodart\nPierre Pictfactory Laloge");
        }
      }
    }
    if (Sup.Input.wasKeyJustPressed("SPACE")) {
      Sup.loadScene("Scenes/MainScene");
    }
  }
}
Sup.registerBehavior(MenuManagerBehavior);
