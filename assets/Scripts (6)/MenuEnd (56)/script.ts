class MenuEndBehavior extends Sup.Behavior {
  
  awake() {
    
  }

  update() {
    if (Sup.Input.isKeyDown('R'))
      Sup.loadScene("Scenes/MainScene");
    if (Sup.Input.isKeyDown('Q'))
      Sup.loadScene("Scenes/MenuScene");
  }
}
Sup.registerBehavior(MenuEndBehavior);
