class SonarBehavior extends Sup.Behavior {
  
  scale : number = 0.01;
  radius : number = 0.4;
  
  awake() {
    this.actor.setZ(1.5);
  }

  detect(target : Sup.Actor) {
    if (target && target.getName() == "Daddy" && target.getBehavior(DadBehavior).triggered <= 0)
      {
        let distance = this.actor.getPosition().distanceTo(target.getPosition());
        if (distance >= (this.scale * 10) && distance < (this.scale * 10) + 2) {
          Sup.appendScene("Prefabs/P_Trigger", target)[0].setParent(Sup.getActor("Map"), { keepLocalTransform: false });
          Sup.Audio.playSound("Sounds/Bip");
        }
        if (target.getName() == "Daddy")
          target.getBehavior(DadBehavior).triggered = 50;
      }
    if (target && target.getName() == "Mummy" && target.getBehavior(MumBehavior).triggered <= 0)
      {
        let distance = this.actor.getPosition().distanceTo(target.getPosition());
        if (distance >= (this.scale * 10) && distance < (this.scale * 10) + 2) {
          Sup.appendScene("Prefabs/P_Trigger", target)[0].setParent(Sup.getActor("Map"), { keepLocalTransform: false });
          Sup.Audio.playSound("Sounds/Bip");
        }
        if (target.getName() == "Mummy")
          target.getBehavior(MumBehavior).triggered = 50;
      }
  }
  
  update() {
    this.scale += 0.003;
    if (this.scale < this.radius) {
      this.actor.setLocalScaleX(this.scale);
      this.actor.setLocalScaleY(this.scale);
      this.actor.spriteRenderer.setOpacity((this.radius - this.scale) / this.radius);      
    } else {
      this.actor.destroy();
    }
    this.detect(Sup.getActor("Daddy"));
    this.detect(Sup.getActor("Mummy"));
  }
}
Sup.registerBehavior(SonarBehavior);
