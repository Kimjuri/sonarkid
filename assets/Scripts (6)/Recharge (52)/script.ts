class RechargeBehavior extends Sup.Behavior {

  gameManager : Sup.Actor;
  player : Sup.Actor;
  
  start() {
    this.gameManager = Sup.getActor("GameManager");
    this.player = Sup.getActor("Player");
  }
  
  update() {
    if (this.player.getX() > (this.actor.getX() - 0.25) && this.player.getX() < (this.actor.getX() + 0.25) &&
        this.player.getY() > (this.actor.getY() - 1) && this.player.getY() < (this.actor.getY() + 0.70)) {
      this.gameManager.getBehavior(TimeManagerBehavior).addMaxTime(1);
    }
  }
}
Sup.registerBehavior(RechargeBehavior);
