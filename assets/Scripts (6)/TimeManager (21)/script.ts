class TimeManagerBehavior extends Sup.Behavior {
  
  // Variable de gestion du temps
  startTime : number;
  currentTime : number;
  oldTime : number;
  maxTime : number = 100;
  
  // Fonction pour modifier la variable maxTime
  addMaxTime(i) {
      this.maxTime += i;
      if (this.maxTime < 0) {
        this.maxTime = 0;
      } else if (this.maxTime > 100) {
        this.maxTime = 100;
      }
  }
  
  awake() {
    // Initialisation de la variable de départ
    let date = new Date();
    this.oldTime = date.getTime();
    delete (date);
  }
  
  update() {
    
    // On prend le temps à chaque tour d'update
    let date = new Date();
    this.currentTime = date.getTime();
    delete (date);
    
    // On vérifie qu'il y a une différence de temps d'au moins 1 seconde depuis le dernier tour d'update avec de décrémenter la variable
    if (((Math.floor(this.currentTime / 1000)) > Math.floor(this.oldTime / 1000))) {
      this.addMaxTime(-1);
    }
    
    // On met à jour la variable de vérification
    this.oldTime = this.currentTime;
    
    // Si la variable de temps est à zero on stop la partie en cours
    if (this.maxTime == 0) {
      Sup.getActor("GameManager").getBehavior(GameManagerBehavior).isEnded = true;
    }
    
    // On modifie le sprite de la batterie pour afficher au joueur le niveau de batterie
    // Nan il n'y a pas de nombre magique tout est normal
    if (Sup.getActor("BatteryFull"))
      Sup.getActor("BatteryFull").setLocalScaleX(0.86 * this.maxTime / 100 + 0.00001);
  }
}

Sup.registerBehavior(TimeManagerBehavior);