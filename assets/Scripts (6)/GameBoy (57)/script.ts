class GameBoyBehavior extends Sup.Behavior {
  
  player : Sup.Actor;
  gameManager : Sup.Actor;
  
  start() {
    this.player = Sup.getActor("Player");
    this.gameManager = Sup.getActor("GameManager");
  }
  
  update() {
    if (this.player.getX() > (this.actor.getX() - 0.3) && this.player.getX() < (this.actor.getX() + 0.5) &&
        this.player.getY() > (this.actor.getY() - 0.7) && this.player.getY() < (this.actor.getY() + 0.3)) {
      Sup.loadScene("Scenes/EndScene");
    }
  }
}
Sup.registerBehavior(GameBoyBehavior);
