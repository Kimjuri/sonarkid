class PropsBehavior extends Sup.Behavior {
  
  player : Sup.Actor;
  touched : number;
  
  awake() {
    this.player = Sup.getActor("Player");
    this.touched = 0;
  }
  
  update() {
    if (this.touched > 0) {
      this.actor.setZ(1.9);
      Sup.getActor("Filter").setVisible(false);      
      this.touched += 0.03;
      this.actor.setLocalScaleX(this.touched);
      this.actor.setLocalScaleY(this.touched);
      if (this.touched > 2) {
        Sup.setTimeout(2000, function () {
          Sup.loadScene("Scenes/LoseEndScene");
        }); 
      }
    }
    if (this.touched == 0 && this.player.getX() > (this.actor.getX() - 0.3) && this.player.getX() < (this.actor.getX() + 0.3) &&
        (this.player.getY() + 0.5) > (this.actor.getY() - 0.3) && (this.player.getY() - 0.7) < (this.actor.getY() + 0.3)) {
      let sprite : string = this.actor.getName();
      if (sprite == "S_Cat") {
        Sup.Audio.playSound("Sounds/ScreamCat");
      }
      if (sprite == "S_Lego") {
        Sup.Audio.playSound("Sounds/ScreamPlayer");
      }
      if (sprite == "S_RubberDucky") {
        Sup.Audio.playSound("Sounds/ScreamRubberDucky");
      }
      if (sprite == "S_Chicken") {
        Sup.Audio.playSound("Sounds/ScreamChicken");
      }
      this.touched = 0.1;
    }
  }
}
Sup.registerBehavior(PropsBehavior);
