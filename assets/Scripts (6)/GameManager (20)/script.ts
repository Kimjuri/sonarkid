class GameManagerBehavior extends Sup.Behavior {
  isEnded : boolean;
  startPos : Sup.Math.Vector3;
  camera : Sup.Camera;
  
  awake() {
      this.isEnded = false;
      this.startPos = new Sup.Math.Vector3(7.388, 6.832, 1);
  }
  
  start(){
    this.camera = Sup.getActor("Camera").camera;
  }
  
  reloadGame() {
    Sup.getActor("Player").arcadeBody2D.warpPosition(this.startPos); 
    Sup.getActor("GameManager").getBehavior(TimeManagerBehavior).maxTime = 100;
    this.isEnded = false;
    Sup.getActor("Filter").setVisible(true);
    Sup.getActor("Camera").setPosition(new Sup.Math.Vector3(4.772, 4.817, 2.1));
  }
  
  update() {
    // Effet de zom inversé
    if (this.camera.getOrthographicScale() < 7) {
      this.camera.setOrthographicScale(Sup.getActor("Camera").camera.getOrthographicScale() + 0.1)
    }

    // Reload le jeu
    if (Sup.Input.isKeyDown("R")) {
      this.reloadGame();
    }
  }
}

Sup.registerBehavior(GameManagerBehavior);