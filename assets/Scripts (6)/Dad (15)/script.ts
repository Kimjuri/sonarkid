class DadBehavior extends Sup.Behavior {
  
  id : number = 1;
  speed : number = 0.01;
  points = [{x:7.11,y:2.41},
            {x:2.47,y:2.41},
            {x:2.47,y:5.34},
            {x:2.47,y:-4.77},
            {x:6.18,y:-4.77},
            {x:2.47,y:-4.77},
            {x:2.47,y:2.00}];
      
  triggered : number;
  
  changeLayout()
  {
    if (this.actor.getX() > 1.3) {
        if (this.actor.getY() > 4.7)
            this.actor.setZ(0.05);
        else if (this.actor.getY() > 3)
          this.actor.setZ(0.15);
        else if (this.actor.getY() > - 2.5)
          this.actor.setZ(0.25);
        else if (this.actor.getY() > -6.20)
          this.actor.setZ(0.35);
        else if (this.actor.getY() > -9)
          this.actor.setZ(0.45);
      } else {
        if (this.actor.getY() > 2.87)
          this.actor.setZ(0.15);
        else if (this.actor.getY() > 1)
          this.actor.setZ(0.25);
        else if (this.actor.getY() > -3.64)
          this.actor.setZ(0.35);
        else if (this.actor.getY() > -9)
          this.actor.setZ(0.45);
    }
  }

  awake() {
    this.triggered = 0;
  }
  
  start() {
      let pointX = this.points[this.id].x;
      let pointY = this.points[this.id].y;
      if (pointX < this.actor.getX()) {
        this.actor.spriteRenderer.setHorizontalFlip(true);
      } else {
        this.actor.spriteRenderer.setHorizontalFlip(false);      
      }
  }

  watch(player : Sup.Actor) {
    let center = new Sup.Math.Vector3(this.points[this.id].x - this.actor.getPosition().x, this.points[this.id].y - this.actor.getPosition().y, 0).normalize().multiplyScalar(2).add(this.actor.getPosition());
    let distance = center.distanceTo(player.getPosition());
    if (distance < 2) {
      Sup.getActor("GameManager").getBehavior(GameManagerBehavior).reloadGame();
    }
  }
    
  movement() {
    let actorX = this.actor.getX();
    let actorY = this.actor.getY();

    let pointX = this.points[this.id].x;
    let pointY = this.points[this.id].y;

    if (((Math.round(actorX * 100) / 100) >= (pointX - 0.01) && (Math.round(actorX * 100) / 100) <= (pointX + 0.01))
        && ((Math.round(actorY * 100) / 100) >= (pointY - 0.01) && (Math.round(actorY * 100) / 100) <= (pointY + 0.01))) {
      this.id = (this.id + 1) % this.points.length;
      pointX = this.points[this.id].x;
      pointY = this.points[this.id].y;
      if (pointX < actorX) {
        this.actor.spriteRenderer.setHorizontalFlip(true);
      } else {
        this.actor.spriteRenderer.setHorizontalFlip(false);      
      }
    }

    if (actorX != pointX) {
      this.actor.arcadeBody2D.setVelocityX(actorX < pointX ? this.speed : this.speed * -1);
    }
    if (actorY != pointY) {
      this.actor.arcadeBody2D.setVelocityY(actorY < pointY ? this.speed : this.speed * -1);
    }

    if (actorY < pointY && actorX > pointX - 0.01 && actorX < pointX + 0.01) {
      if (this.actor.spriteRenderer.getAnimation() != "WalkBack") {
        this.actor.spriteRenderer.setAnimation("WalkBack");        
      }
    } else {
      if (actorY > pointY && actorX > pointX - 0.01 && actorX < pointX + 0.01) {
        if (this.actor.spriteRenderer.getAnimation() != "WalkFace") {
          this.actor.spriteRenderer.setAnimation("WalkFace");
        }
      } else {
        this.actor.spriteRenderer.setAnimation("WalkSide");
      }
    }
  }
  
  update() {
    Sup.ArcadePhysics2D.collides(this.actor.arcadeBody2D, Sup.ArcadePhysics2D.getAllBodies());
    if (this.triggered >= 0)
      this.triggered--;
    if (Sup.getActor("GameManager").getBehavior(GameManagerBehavior).isEnded == false) {
      this.movement();
      this.changeLayout();
      this.watch(Sup.getActor("Player"));
    }
  }
}

Sup.registerBehavior(DadBehavior);
