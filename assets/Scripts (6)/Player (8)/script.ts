class PlayerBehavior extends Sup.Behavior {
  
  speed : number = 0.05;
  camera : Sup.Actor;
  
  start() {
    this.camera = Sup.getActor("Camera");
  }
  
  changeLayout()
  {
    if (this.actor.getX() > 1.3) {
        if (this.actor.getY() > 4.7)
            this.actor.setZ(0.05);
        else if (this.actor.getY() > 3)
          this.actor.setZ(0.15);
        else if (this.actor.getY() > - 2.5)
          this.actor.setZ(0.25);
        else if (this.actor.getY() > -6.20)
          this.actor.setZ(0.35);
        else if (this.actor.getY() > -9)
          this.actor.setZ(0.45);
      } else {
        if (this.actor.getY() > 2.87)
          this.actor.setZ(0.15);
        else if (this.actor.getY() > 1)
          this.actor.setZ(0.25);
        else if (this.actor.getY() > -3.64)
          this.actor.setZ(0.35);
        else if (this.actor.getY() > -9)
          this.actor.setZ(0.45);
    }
  }

  moveCamera() {
    if (this.actor.getX() > -4.7 && this.actor.getX() < 4.7)
      this.camera.setX(this.actor.getX());    
    if(this.actor.getY() > -4.7 && this.actor.getY() < 4.7)
      this.camera.setY(this.actor.getY());
  }
  
  player_movement() {
    Sup.ArcadePhysics2D.collides(this.actor.arcadeBody2D, Sup.ArcadePhysics2D.getAllBodies());  
    if (Sup.Input.isKeyDown("RIGHT") || Sup.Input.isKeyDown("D")) {
      this.actor.spriteRenderer.setHorizontalFlip(false);
      this.actor.arcadeBody2D.addVelocityX(this.speed);
      if (this.actor.spriteRenderer.getAnimation() != "WalkRight")
        this.actor.spriteRenderer.setAnimation("WalkRight").playAnimation();
    } else if (Sup.Input.isKeyDown("LEFT") || Sup.Input.isKeyDown("Q")) {
      this.actor.spriteRenderer.setHorizontalFlip(true);
      this.actor.arcadeBody2D.addVelocityX(-this.speed);
      if (this.actor.spriteRenderer.getAnimation() != "WalkRight")
        this.actor.spriteRenderer.setAnimation("WalkRight").playAnimation();
    } else if (Sup.Input.isKeyDown("UP") || Sup.Input.isKeyDown("Z")) {
      this.actor.arcadeBody2D.addVelocityY(this.speed);
      if (this.actor.spriteRenderer.getAnimation() != "WalkBack")
        this.actor.spriteRenderer.setAnimation("WalkBack").playAnimation();
    } else if (Sup.Input.isKeyDown("DOWN") || Sup.Input.isKeyDown("S")) {
      this.actor.arcadeBody2D.addVelocityY(-this.speed);
      if (this.actor.spriteRenderer.getAnimation() != "WalkFace")
        this.actor.spriteRenderer.setAnimation("WalkFace").playAnimation();
    } else {
      if (this.actor.spriteRenderer.getAnimation() != "Idle")
        this.actor.spriteRenderer.setAnimation("Idle").playAnimation();
    }
    let vector : Sup.Math.Vector2 = this.actor.arcadeBody2D.getVelocity();
    vector.x *= 0.5;
    vector.y *= 0.5;
    this.actor.arcadeBody2D.setVelocity(vector);
  }

  pop_sonar() {
    if (Sup.Input.wasKeyJustPressed("SPACE") && Sup.getActor("GameManager").getBehavior(TimeManagerBehavior).maxTime > 0) {
      Sup.appendScene("Prefabs/P_Sonar", this.actor)[0].setParent(Sup.getActor("Map"), { keepLocalTransform: false });
      Sup.Audio.playSound("Sounds/Boup");
      Sup.getActor("GameManager").getBehavior(TimeManagerBehavior).addMaxTime(-10);
    }
    if (Sup.getActor("GameManager").getBehavior(TimeManagerBehavior).maxTime <= 0) {
      Sup.getActor("GameManager").getBehavior(GameManagerBehavior).reloadGame();      
    }
  }
  
  update() {
    if (Sup.getActor("GameManager").getBehavior(GameManagerBehavior).isEnded == false) {
      this.player_movement();
      this.changeLayout();
      this.pop_sonar();
      this.moveCamera();
    } else {
      this.actor.arcadeBody2D.setVelocity(0, 0);
    }
    if (Sup.Input.isKeyDown('G')) {
      Sup.log("(" + this.actor.getX() + ", " + this.actor.getY() + ")");
    }
  }
}
Sup.registerBehavior(PlayerBehavior);